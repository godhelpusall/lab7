#include <iostream>
using namespace std;

int main()
{
  double a = 2.4;
  double* ptr_to_double = &a;
  cout << "Variable value: " << a << ";";
  cout << "address : " << &ptr_to_double<< "\n";
  cout << "Pointer value: " << ptr_to_double;
  cout << "; Pointer Address: " << &ptr_to_double;
  cout << "; dereference: " << *ptr_to_double;
  
  
}
